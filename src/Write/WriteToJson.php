<?php
namespace Cat\Challenge\Write;

use Exception;

/**
 * Class WriteToJson
 * @package Cat\Challenge\Readers\Write
 */
class WriteToJson
{
    /**
     * @var string
     */
    private $filename;

    /**
     * WriteToJson constructor.
     * @param $filename
     */
    function __construct($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @param $output
     * @throws Exception
     */
    public function writeLog($output)
    {
        $outgoing = fopen($this->filename, 'w');
        if(fwrite($outgoing,json_encode($output)) !== false)
        {
            echo 'Saved to output.json file.';
            fclose($outgoing);
        }
        else
        {
            throw new Exception('Something went wrong! File not saved.');
        }
    }
}