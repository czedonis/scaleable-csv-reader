<?php
namespace Cat\Challenge\Read\Adaptors;
use Exception;

/**
 * Class PolicyFactory
 * @package Cat\Challenge\Read\Adaptors
 */
class PolicyFactory
{
    /**
     * @param $filename
     * @return PolicyIterator
     * @throws Exception
     */
    public static function load($filename): PolicyIterator
    {
        $filetype = mime_content_type($filename);

        switch($filetype)
        {
            case 'text/csv':
            case 'application/vnd.ms-excel':
            case 'text/tsv':
            case 'text/plain' && (pathinfo($filename, PATHINFO_EXTENSION) == 'csv'):
                 $adapter = new CsvAdapter($filename);
            break;
            default:
                throw new Exception('This Filetype is unsupported at this time. Please use a csv');
        }
        return new PolicyIterator($adapter);
    }
}