<?php
namespace Cat\Challenge\Read\Adaptors;

/**
 * Interface ReaderAdapter
 * @package Cat\Challenge\Read\Adaptors
 */
interface ReaderAdapter
{
    /**
     * @return array
     */
    public function getFileAsArray() : array;

    /**
     * @param string $filename
     * @return mixed
     */
    public function setFilePath(string $filename);

    /**
     * @return mixed
     */
    public function getFilePath();
}