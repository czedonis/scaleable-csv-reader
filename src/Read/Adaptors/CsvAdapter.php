<?php
namespace Cat\Challenge\Read\Adaptors;

use Exception;

Class CsvAdapter implements ReaderAdapter
{
    /**
     * @var string
     */
    private $filename ='';

    /**
     * CsvAdapter constructor.
     * @param $filename
     */
    public function __construct($filename)
    {
        $this->setFilePath($filename);
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getFileAsArray() : array
    {
        ini_set("auto_detect_line_endings", true);
        $csvArray = [];
        if (($handle = fopen($this->filename, "r")) === FALSE) {
            throw new Exception('File failed to open.');
        }

        while(($data = fgetcsv($handle)) !== FALSE)
        {
            $csvArray[] = $data;
        }
        fclose($handle);
        return $csvArray;
    }

    /**
     * @param string $filename
     */
    public function setFilePath(string $filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
       return $this->filename;
    }
}