<?php

namespace Cat\Challenge\Read\Adaptors;

use Iterator;

/**
 * single iteration iterator
 * Class PolicyIterator
 * @package Cat\Challenge\Read\Adaptors
 *
 */
class PolicyIterator implements Iterator
{
    /**
     * @var int
     */
    private $position;

    /**
     * @var CsvAdapter
     */
    private $adaptor;

    /**
     * PolicyIterator constructor.
     * @param ReaderAdapter $adaptor
     */
    public function __construct(ReaderAdapter $adaptor) {
        $this->adaptor = $adaptor->getFileAsArray();
        $this->position = 0;
    }

    /**
     * empty so it doesn't reset after we pull out the header(at pos 0)
     * since rewind is called first before first loop
     */
    public function rewind() {
    }

    /**
     * @return mixed
     */
    public function current() {
        return $this->adaptor[$this->position];
    }

    /**
     * @return bool|float|int|string|null
     */
    public function key() {
        return $this->position;
    }

    /**
     * progress to next
     */
    public function next() {
        ++$this->position;
    }

    /**
     * @return bool
     */
    public function valid(): bool
    {
        return isset($this->adaptor[$this->position]);
    }
}