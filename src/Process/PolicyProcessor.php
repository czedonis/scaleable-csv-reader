<?php
namespace Cat\Challenge\Process;

use Cat\Challenge\Read\Adaptors\PolicyIterator;
use Exception;

/**
 * Class PolicyProcessor
 * @package Cat\Challenge\Readers\Process
 */
class PolicyProcessor
{
    /**
     * @var array
     */
    private $policyArray;

    /**
     * @var array
     */
    private $headers;

    /**
     * PolicyProcessor constructor.
     * @param PolicyIterator $iterator
     */
    function __construct(PolicyIterator $iterator)
    {
        $valueArray = $iterator;
        $this->headers = $valueArray->current();
        $valueArray->next();
        $this->policyArray = $valueArray;
    }

    /**
     * @param $fields
     * @param $incrementalField
     * @return array
     * @throws Exception
     */
    public function processFields($fields,$incrementalField): array
    {
        $output = [];
        $incrementalIndex = $this->getFieldKeysByName($incrementalField);
        if(empty($fields))
            throw new Exception('required fields missing');

        foreach ($this->policyArray as $key => $policy) {
            foreach($fields as $field)
            {
                $fieldIndex = $this->getFieldKeysByName($field);
                if(isset($countyT12[$policy[$field]])) {
                    $output[$field][$policy[$fieldIndex]][$incrementalField] += $policy[$incrementalIndex];
                }
                else
                {
                    $output[$field][$policy[$fieldIndex]] = [$incrementalField => $policy[$incrementalIndex]];
                }
            }
        }

        return $output;
    }


    /**
     * @param $name
     * @return int
     * @throws Exception
     */
    private function getFieldKeysByName($name): int
    {
        if($returnMe = array_search($name,$this->headers))
        {
            return $returnMe;
        }
        else
        {
            throw new Exception('CSV missing expected field: ' . $name);
        }
    }

}