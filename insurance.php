<?php
namespace Cat\Challenge;

require __DIR__ . '/vendor/autoload.php';

use Cat\Challenge\Process\PolicyProcessor;
use Cat\Challenge\Read\Adaptors\PolicyFactory;
use Cat\Challenge\Write\WriteToJson;
use Exception;

try {
    $csvFile = PolicyFactory::load("FL_insurance_sample.csv");
    $processor = new PolicyProcessor($csvFile);
    $output = $processor->processFields(array('line','county'),'tiv_2012');
    $logOutput = new WriteToJson("output.json");
    $logOutput->writeLog($output);

} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}